function [ so ] = rots( D )
%ROTS generate the lie algebra so(D) of SO(D)

n = D*(D-1)/2;
so = zeros(D,D,n);

k = 0;
for i=1:D
    for j=i+1:D
        k = k +1;
        so(i,j,k) = 1;
        so(j,i,k) = -1;
    end
end
