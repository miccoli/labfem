function [ K ] = kbar( XY, klin)
%kbar compute stifness matrix of a BAR element
%   k = kbar(XY, kk) returns the stifness matrix of a
%   bar element with nodes at XY and stiffness klin
%
%   XY(ND,2) is a ND x 2 matrix whose columns are the coordinates
%   of the BAR element; ND = 2
%   klin is a scalar

[ND, nnodel] = size(XY);

% check input args
assert(nnodel==2);
assert(isscalar(klin));

% number of degrees of freedom
dof = ND*nnodel;

% compute rotation matrix from global ref. frame to local
dxy = (XY(:,2) - XY(:,1))';
dxy = dxy / norm(dxy);
R = [dxy; null(dxy)'];

% block matrix rotation matrix
RR = zeros(dof,dof);
RR(1:ND,1:ND) = R;
RR(ND + (1:ND), ND + (1:ND)) = R;

% stiffness matrix in local ref. frame
K = zeros(dof, dof);
K(1,1) = klin;
K(1,ND+1) = -klin;
K(ND+1,1) = -klin;
K(ND+1,ND+1) = klin;

% transform into global ref. frame
K = RR'*K*RR;

end
