%% Esercitazione massa-molla

% dati scalari
% k  : rigidezza molla (N/m)
% mm : massa molla (kg)
% ms : massa sospesa (kg)

% dati vettoriali:
% n : per ogni elemento n_i di n si effettua un esperimento numerico
%     suddividendo la molla in n_i elementi 
n = unique(round(logspace(0, 3, 31)));

% cerca gli indici degli esperimenti numerici con 1, 10, 100 elementi
id = find(n==1 | n==10 | n==100 );

%% vibrazioni libere della molla, no vincoli, no massa sospesa

k = 1;
mm = 1;

% soluzione analitica
w2a = pi^2 * k / mm; % omega^2 analitico
xol = linspace(-1/2, 1/2, 1000); % x/l
van = sin(pi*xol);

w2n = [];   % omega^2 numerico
vn = {};    % xn, vn: coordinate e valori nodali del modo di vibrare
xn = {};
t = [];     % tempi impiegati
for i = 1:length(n)
    tic
    [K,M] = km(k, mm, n(i));
    [V,D] = eig(K,M);
    % il primo autovalore è nullo
    assert(abs(D(1,1)) < w2a * 1e-8)
    w2n(i) = D(2,2);
    xn{i} = linspace(-1/2, 1/2, n(i)+1);
    vn{i} = V(:,2) ./ V(end, 2);
    t(i) = toc;
end
%% plot dei risultati

figure()
loglog(n, abs(w2n-w2a)/w2a, 'o-')
title('Molla libera: convergenza')
xlabel('Numero di elementi')
ylabel('errore relativo')
grid()

figure()
loglog(n, t, 'o-')
title('Molla libera: costo computazionale')
xlabel('Numero di elementi')
ylabel('tempo trascorso per singola iterazione')
grid()

figure()
plot(xol, van, 'DisplayName', 'analitico')
hold;
for i = 1:2:5
    plot(xn{i}, vn{i}, 'o-', 'DisplayName', sprintf('%d el.', n(i)))
end
title('Molla libera: primo modo di vibrare')
xlabel('x / l')
ylabel('u')
legend('Location', 'NorthWest')
grid()

%% primo estremo vincolato; massa ms sospesa a secondo estremo

ms = 95.77e-3 + 0.25e-3;
mm = 57.53e-3;
k = 6.23;

% calcolo soluzione analitica
fun = @(x) x * tan(x) * ms - mm;
alpha = fzero(fun, [0, pi/2]);
w2a = alpha^2 * k / mm;
xol = linspace(0, 1, 1000); 
van = sin(alpha*xol) / sin(alpha);

w2n = [];
vn = {};
xn = {};
t = [];
for i = 1:length(n)
    tic
    [K,M] = km(k,mm,n(i));
    M(end, end) = M(end, end) + ms;
    [V, D] = eig(K(2:end,2:end), M(2:end,2:end));
    w2n(i) = D(1,1);
    xn{i} = linspace(0, 1, n(i)+1);
    vn{i} = [0; V(:,1)] / V(end, 1);
    t(i) = toc;
end

%% plot dei risultati

figure()
loglog(n, abs(w2n-w2a)/w2a, 'o-')
title('Massa sospesa: convergenza')
xlabel('Numero di elementi')
ylabel('errore relativo')
grid()

figure()
loglog(n, t, 'o-')
title('Massa sospesa: costo computazionale')
xlabel('Numero di elementi')
ylabel('tempo trascorso per singola iterazione')
grid()

figure()
plot(xol, van, 'DisplayName', 'analitico')
hold;
for i = 1:2:5
    plot(xn{i}, vn{i}, 'o-', 'DisplayName', sprintf('%d el.', n(i)))
end
title('Massa sospesa: primo modo di vibrare')
xlabel('x / l')
ylabel('u')
legend('Location', 'NorthWest')
grid()

%% stampa dei risultati
% funzione di utilità per calcolare il periodo
w2toT = @(x) 1./ (x.^0.5 / 2 / pi);


fprintf("\nMassa sospesa\n\n")
fprintf("Soluzione analitica: %.4f\n", w2toT(w2a));
fprintf("Soluzione numerica per %d el.: %.4f\n", ...
    [[1 10 100]; w2toT(w2n(id))]);