function [K, M] = km(k, m, n)
%KM matrice di rigidezza e di massa per una molla divisa in n elementi
%
%   [K, M] = km(k, m, n)
%   discretizza una molla di rigidezza k e massa m in un sistema di n
%   molle eguali tra loro e ne calcola le matrici di rigidezza e di massa.

if n <= 0 || fix(n) ~= n
    error('il parametro n deve essere un intero positivo')
end

ki = n * k;
mi = m / n;

d = ones(n, 1);

K = diag(-ki * d, 1);
K(1, 1) = ki;
K(end, end) = ki;
K = K + diag(2 * ki * [0; d(1:end-1); 0]);
K = K + diag(-ki * d, -1);

M = diag(mi / 6 * d, 1);
M(1, 1) = mi / 3;
M(end, end) = mi / 3;
M = M + diag(2 * mi / 3 * [0; d(1:end-1); 0]);
M = M + diag(mi / 6 * d, -1);

end
