function [K, M] = kms(k, m, n)
%KM matrice di rigidezza e di massa per una molla divisa in n elementi
%   formulazione sparsa
%
%   [K, M] = kms(k, m, n)
%   discretizza una molla di rigidezza k e massa m in un sistema di n
%   molle eguali tra loro e ne calcola le matrici di rigidezza e di massa
%   salvate in formato sparso.

if n <= 0 || fix(n) ~= n
    error('parameter n must be a positive integer')
end

ki = n * k;
mi = m / n;

d = ones(n + 1, 1);

K = ki * spdiags(...
    [-d, [1; 2 * d(1:end-2); 1], -d], ...
    [-1, 0, 1], ...
    n + 1, n + 1);

M = mi * spdiags(...
    [d / 6,[1; 2 * d(1:end-2); 1] / 3, d / 6], ...
    [-1, 0, 1], ...
    n + 1, n + 1);

end
